#!/usr/bin/python
import sys, json, xmltodict, dicttoxml



if __name__=='__main__':
    args = []
    for arg in sys.argv:
        args.append(arg)
##    print("args[1] is "),
##    print args[1]

    if (args[1] != "-j" and args[1] != "-x"):
       print("Usage is convert.py -j <json file name> to convert json to xml")
       print("or covert.py -x <xml file name> to convert xml to json")

    print("Reading "),

    if args[1] == "-j":
        print(args[2])
        with open(args[2]) as json_file:
            json_dict = json.load(json_file)
            xml = dicttoxml.dicttoxml(json_dict)
            output = open("output.xml", "w")
            output.write(xml)
            #print(json_dict)

    if args[1] == "-x":
        print(args[2])
        xml_file = open(args[2], "rb")
        xml_dict = xmltodict.parse(xml_file, xml_attribs=True)
        #print(xml_dict)
        output = open("output.json", "w")
        json = json.dumps(xml_dict)
        output.write(json)
